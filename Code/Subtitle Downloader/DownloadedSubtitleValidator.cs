﻿using System.IO;
using System.Net;
using System.Threading;
using AMSI.ImportEngine;
using AVSI.ImportEngine;
using MeediOS;


namespace AMSI.SubtitleDownloader
{



    class DownloadedSubtitleValidator
    {




        internal static bool ValidateDownloadedDataAndRetry
            (string language,  IMLItem item, string firstsub,
             WebClient client, string zipfilePath)
        {

            StreamReader sr
                = File.OpenText(zipfilePath);


            if (CheckFirstLineForHtmlToken(sr))
                return true;

            Sleep10SecsToRetry(sr);

            File.Delete(zipfilePath);



            client.DownloadFile(firstsub, zipfilePath);

            sr = File.OpenText(zipfilePath);

            if (!CheckFirstlineForHtmlTokenFinalTry
                (zipfilePath, sr))
                return false;



            MainImportingEngine.ThisProgress.Progress
                (ImportProgress.CurrentProgress,
                 "Subtitle for " + item.Name +
                 " was downloaded succesfully.");


            Thread.Sleep(1500);

            item.Tags
                ["Subtitle language"]
                = language;
          

            item.SaveTags();

            sr.Close();
            sr.Dispose();

            return true;

        }



        private static bool CheckFirstlineForHtmlTokenFinalTry
            (string zipfilePath, TextReader sr)
        {

            string zipfileLine = sr.ReadLine();


            if (zipfileLine != null
                && zipfileLine.Contains("DOCTYPE html"))
            {

                MainImportingEngine.ThisProgress.Progress
                    (ImportProgress.CurrentProgress,
                     "Unable to extract subtitle." +
                     " The downloaded archive was corrupt.");


                sr.Close();
                Thread.Sleep(1500);
                File.Delete(zipfilePath);

                return false;
            }


            return true;
        }




        private static bool CheckFirstLineForHtmlToken(TextReader sr)
        {
            string zipfileLine
                = sr.ReadLine();


            return zipfileLine == null
                   || !zipfileLine.Contains
                           ("DOCTYPE html");
        }


        private static void Sleep10SecsToRetry(TextReader sr)
        {
            MainImportingEngine.ThisProgress.Progress
                (ImportProgress.CurrentProgress,
                 "The subtitle archive was corrupt." +
                 " Retrying in 10 seconds...");

            sr.Close();
            Thread.Sleep(10000);
        }





    }


}
