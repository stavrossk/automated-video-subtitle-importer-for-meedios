﻿using System;
using System.IO;
using AMSI.ImportEngine;
using AMSI.SubtitleDownloader;
using AVSI.SubtitleDownloader;
using AVSI.ImportEngine;
using MeediOS;



namespace AVSI.OSDb
{

    internal class VideoSubtitleDownloaderHelpers
    {


        internal static void SearchForAndDownloadSubtitleForPrimaryOrSecondaryLanguage
            (IMLItem item)
        {


            if (CheckForExistingSubtitle(item)) 
                return;

            string videoHash = Helpers.GetTagValueFromItem(item, "Hash");

            MainImportingEngine.ThisProgress.Progress
                (ImportProgress.CurrentProgress,
                 "Searching subtitles for " + item.Name + "...");



            bool foundSubtitleForPrimaryLanguage
                = SearchForAndDownloadSubtitleInSpecifiedLanguage
                (videoHash, Settings.PrimaryLanguage, item);


            if (!foundSubtitleForPrimaryLanguage)
                SearchForAndDownloadSubtitleInSpecifiedLanguage
                    (videoHash,
                     Settings.SecondaryLanguage,
                     item);

        }


        private static bool CheckForExistingSubtitle(IMLItem item)
        {
            string location = item.Location;


            string subtitleFullPath 
                = ConstructSubtitlePath
                (location, ".srt", string.Empty);


            if (File.Exists(subtitleFullPath))
                return true;



            subtitleFullPath 
                = ConstructSubtitlePath
                (location, ".sub", string.Empty);

            if (File.Exists(subtitleFullPath))
                return true;
            
            return false;
        }



        internal static bool SearchForAndDownloadSubtitleInSpecifiedLanguage
            (
            string videoHash,
            string language,
            IMLItem item
            )
        {

            #region  INIT

            string videoFileLocation = item.Location;

            #endregion



            try
            {


                string subtitleZipFileUrl
                    = OsDbSubtitleMatcher
                    .SearchForSubtitleByVideoHashParent
                    (videoHash, language);



                SubtitleDownloadingEngine.RetrieveSubtitleFromZipfileUrl
                    (subtitleZipFileUrl, videoFileLocation, language);


            }
            catch (Exception e)
            {

                Helpers.LogMessageToFile
                    ("An unexpected error occured in the core subtitle downloading engine. " +
                     "The error was: " + e);

                return false;

            }



            return true;


        }




        internal static string ConstructSubtitlePath
            (string location,
            string subtitleFileExtension,
            string subtitleLanguage)
        {

            var locationFi =
                new FileInfo(location);

            string videoFilename = locationFi.Name;
            string videoFilenameParentPath = locationFi.DirectoryName;

            if (videoFilenameParentPath == null)
                return String.Empty;

            string subtitleFilename =
                videoFilename.Remove(videoFilename.Length - 4);



            subtitleFilename
                = subtitleFilename
                + "." + subtitleLanguage;



            subtitleFilename 
                = subtitleFilename 
                + subtitleFileExtension;


            string subtitleFileFullPath 
                = Path.Combine
                (videoFilenameParentPath,
                subtitleFilename);


            return subtitleFileFullPath;


        }




    }


}
