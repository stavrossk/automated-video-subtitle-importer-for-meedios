﻿


using CookComputing.XmlRpc;

namespace AVSI
{
    
    public interface IOpenSubtitlesRemoteFunctions : IXmlRpcProxy
    {
        
        [XmlRpcMethod("LogIn")]
        LoginResult LogIn(string username, string password, string language, string useragent);

        [XmlRpcMethod("ServerInfo")]
        XmlRpcStruct ServerInfo();

        [XmlRpcMethod("CheckMovieHash")]
        XmlRpcStruct CheckMovieHash(string moviehash);

        [XmlRpcMethod("CheckMovieHash2")]
        XmlRpcStruct CheckMovieHash2(string moviehash);


        [XmlRpcMethod("SearchSubtitles")]
        XmlRpcStruct SearchSubtitles(string token, SearchParams[] ms);

        [XmlRpcMethod("CheckMovieHash")]
        XmlRpcStruct CheckMovieHash(string token, string[] moviehash);



    }
 

}
