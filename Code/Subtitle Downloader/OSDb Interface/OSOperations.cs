﻿




using System;
using CookComputing.XmlRpc;



namespace AVSI.OSDb
{

    public class OSoperations
    {
        private const string TxtUrl = "http://api.opensubtitles.org/xml-rpc";

        internal static IOpenSubtitlesRemoteFunctions Proxy;

        public static string Token = "4782378472834782378372";


        public static bool ServerInfo()
        {
            CreateProxy();

            try
            {
                Proxy.ServerInfo();
                return true;
            }
            catch (Exception ex)
            {
                Helpers.LogMessageToFile(ex.Message);
                return false;
            }
        }


        public LoginResult SiteLogin()
        {
            CreateProxy();
            //const string userAgent = "Mozilla Firefox";
            LoginResult loginresult = Proxy.LogIn(string.Empty, string.Empty,"eng", Proxy.UserAgent);
            return loginresult;
        }


        private static void CreateProxy()
        {
            Proxy = XmlRpcProxyGen.Create<IOpenSubtitlesRemoteFunctions>();
            Proxy.Timeout = 20000;

            Proxy.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.4) " +
                              "Gecko/20070515 Firefox/2.0.0.4";
            Proxy.Url = TxtUrl;
        }



    }


}
