﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Xml;
using AMSI.ImportEngine;



namespace AMSI.OSDb
{

    class OsdbCommunicator
    {


        internal static bool MakeOsDbRequest
            (string useragent, string searchstring,
             out string subtitleData, out WebResponse webResponse)
        {

            HttpWebRequest request = 
                (HttpWebRequest) WebRequest.Create(searchstring);
            
            request.UserAgent = useragent;
            request.Timeout = 10000;

            try
            {
                webResponse = request.GetResponse();
            }
            catch (Exception)
            {

                ImportProgress.UpdateProgress("", "OSdb did not respond. Retrying...", null);
                Thread.Sleep(2000);

                try
                {
                    webResponse = request.GetResponse();
                }
                catch (Exception)
                {
                    ImportProgress.UpdateProgress("", "OSdb did not respond. Retrying...", null);
                    Thread.Sleep(2000);

                    try
                    {
                        webResponse = request.GetResponse();
                    }
                    catch (Exception)
                    {

                        ImportProgress.UpdateProgress("", "Unable to communicate with OSdb.", null);
                        Thread.Sleep(700);
                        {
                            subtitleData = String.Empty;
                            webResponse = null;
                            return true;
                        }

                    }

                }

            }


            subtitleData = null;
            return false;
        
        }



        internal static bool LoadOsDbResponseXmlDocument
            (XmlDocument xDoc,  WebResponse webResponse, out string subtitleData)
        {

            Stream stream = webResponse.GetResponseStream();

            try
            {
                if (stream != null)
                    xDoc.Load(stream);
            }
            catch (Exception)
            {
                ImportProgress.UpdateProgress
                    ("", "Unable to download subtitle. " +
                         "The response from OSdb was invalid.", null);

                Thread.Sleep(2000);
                subtitleData = String.Empty;
                return true;
            }


            subtitleData = String.Empty;
            return false;
        
        }



        internal static bool MakeOsDbRequestGetResponse
            (string searchstring, XmlDocument xDoc, 
             string useragent, out string subtitleData)
        {
            WebResponse webResponse;

            string subtitleDataA;

            if (MakeOsDbRequest(useragent, searchstring,
                                                 out subtitleDataA, out webResponse))
            {
                subtitleData = subtitleDataA;
                return true;
            }


            string subtitleDataB;

            if (LoadOsDbResponseXmlDocument(xDoc, webResponse, out subtitleDataB))
            {
                subtitleData = subtitleDataB;
                return true;
            }

            subtitleData = null;
            return false;


        }



        internal static XmlNodeList DefineSubtitleXmlNodeElements
            (XmlDocument xDoc, out XmlNodeList subrating, 
             out XmlNodeList download)
        {
            XmlNodeList subtitle = xDoc.GetElementsByTagName("subtitle");
            download = xDoc.GetElementsByTagName("download");
            subrating = xDoc.GetElementsByTagName("subrating");

            return subtitle;
        }



    }




}
