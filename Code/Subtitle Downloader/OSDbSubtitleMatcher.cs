﻿using System;
using System.Threading;
using System.Xml;
using AMSI.ImportEngine;
using AMSI.OSDb;
using AVSI.ImportEngine;

namespace AVSI.SubtitleDownloader
{

    class OsDbSubtitleMatcher
    {

        internal static string SearchForSubtitleByVideoHash(string moviehash, string language)
        {


            #region local variables

            const string prefix = "http://www.opensubtitles.org/search/sublanguageid-";
            string searchstring = prefix + language + "/moviehash-" + moviehash + "/simplexml";

            const string useragent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.4) Gecko/20070515 Firefox/2.0.0.4";
            XmlDocument xDoc = new XmlDocument();
            #endregion


            string subtitleData;

            if (OsdbCommunicator.MakeOsDbRequestGetResponse
                (searchstring, xDoc, useragent, out subtitleData))
                return subtitleData;

            XmlNodeList subrating;
            XmlNodeList download;

            var subtitle = OsdbCommunicator.DefineSubtitleXmlNodeElements
                (xDoc, out subrating, out download);


            #region decide which subtitle to download

            decimal maxRating = 0;
            int preferred = 0;

            for (int i = 0; i < subtitle.Count; i++)
            {
                string sublink = download[i].InnerText;
                Helpers.LogMessageToFile("Subtitle link: " + sublink);

                decimal rating = GetSubtitleRating(i, subrating);

                if (rating <= maxRating)
                    continue;
                
                preferred = i;
                maxRating = rating;
            
            }
            #endregion

            if (download.Count > 0)
            {
                string downloadlink = download[preferred].InnerText;

                if (downloadlink.StartsWith("/download"))
                    downloadlink = "http://www.opensubtitles.org" + downloadlink;

                return downloadlink;

            }

            return null;
        
        }

        internal static string SearchForSubtitleByVideoHashParent
            (string videoHash, string language)
        {

            string firstsub = String.Empty;

            if (!String.IsNullOrEmpty(videoHash))
            {
                MainImportingEngine.ThisProgress.Progress(ImportProgress.CurrentProgress,
                                                          "Searching OSdb for subtitle (by video hash)...");


                firstsub = SearchForSubtitleByVideoHash
                    (videoHash, language);

            }
            else
            {

                MainImportingEngine.ThisProgress.Progress
                    (ImportProgress.CurrentProgress,
                    "Unable to search for video subtitle. " +
                    "Video fingerprint is unknown.");

                Thread.Sleep(2000);

            }

            return firstsub;


        }


        private static decimal GetSubtitleRating
            (int i, XmlNodeList subrating)
        {

            decimal rating;

            if (String.IsNullOrEmpty(subrating[i].InnerText))
            {
                rating = 0;
                return rating;
            }


            try
            {
                rating = Convert.ToDecimal(subrating[i].InnerText) / 10;
            }
            catch (Exception)
            {
                rating = 0;
            }

            Helpers.LogMessageToFile("Subtitle rating: " + rating);
            
            return rating;

        }//endof func


    }//endof class


}
