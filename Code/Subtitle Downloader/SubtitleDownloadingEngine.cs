﻿using System;
using System.IO;
using System.Net;
using AVSI;
using AVSI.OSDb;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;

namespace AMSI.SubtitleDownloader
{
    internal class SubtitleDownloadingEngine
    {




        internal static void RetrieveSubtitleFromZipfileUrl
            (string subtitleZipFileUrl,
            string videoFileLocation,
            string subtitleLanguage)
        {


            if (ValidateSubtitleZipFileUrl(subtitleZipFileUrl))
                return;


            var zipInputStream 
                = LoadZipFileDataFromUrlAndCreateZipInputStream
                (subtitleZipFileUrl);


            ScanZipInputStreamEntriesAndExtractFoundSubtitleFile
                (videoFileLocation, zipInputStream, subtitleLanguage);
        }




        private static void ScanZipInputStreamEntriesAndExtractFoundSubtitleFile
            (string videoFileLocation, ZipInputStream zipInputStream, string subtitleLanguage)
        {


            ZipEntry zipEntry 
                = zipInputStream.GetNextEntry();


            while (zipEntry != null && zipEntry.IsFile)
            {
                String entryFileName = zipEntry.Name;
                // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                // Optionally match entrynames against a selection list here to skip as desired.
                // The unpacked length is available in the zipEntry.Size property.
                FileInfo fi = new FileInfo(entryFileName);
                string ext = fi.Extension;
                string name = fi.Name;


                string subtitleFilename = DetectSubtitleInZipEntry(zipEntry, ext, name);

                CreateSubtitleExtractionPathAndExtractSubtitle
                    (videoFileLocation, zipInputStream,
                    subtitleFilename, subtitleLanguage);

                zipEntry = zipInputStream.GetNextEntry();
            } //endof while
        }


        private static ZipInputStream 
            LoadZipFileDataFromUrlAndCreateZipInputStream
            (string subtitleZipFileUrl)
        {

            var webClient = new WebClient();

            Stream subtitleZipFileData 
                = webClient.OpenRead
                (subtitleZipFileUrl);


            // This stream cannot be opened with the ZipFile class because CanSeek is false.
            var zipInputStream 
                = new ZipInputStream
                    (subtitleZipFileData);
            
            
            return zipInputStream;
        }



        private static bool ValidateSubtitleZipFileUrl
            (string subtitleZipFileUrl)
        {

            Helpers.LogMessageToFile
                ("subtitleZipFileUrl: " + subtitleZipFileUrl);

            if (!String.IsNullOrEmpty(subtitleZipFileUrl))
                return false;
            
            Helpers.LogMessageToFile("AVSI was unable to retrieve a subtitle for this item" +
                " because the received Subtitle zip file's URL was empty.");

            return true;
        }




        private static void CreateSubtitleExtractionPathAndExtractSubtitle
            (string videoFileLocation,
            Stream zipInputStream,
            string subtitleFilename,
            string subtitleLanguage)
        {

            if (String.IsNullOrEmpty(subtitleFilename)) 
                return;
            

            var subtitleFileInfo
                = new FileInfo(subtitleFilename);
            
            string subtitleFileExtension
                = subtitleFileInfo.Extension;


            string fullSubtitlePath 
                = VideoSubtitleDownloaderHelpers.ConstructSubtitlePath
                (videoFileLocation, subtitleFileExtension, subtitleLanguage);


            ExtractSubtitleFromZipStream(zipInputStream, fullSubtitlePath);
        
        }



        private static void ExtractSubtitleFromZipStream
            (Stream zipInputStream, string fullSubtitlePath)
        {
            byte[] buffer = new byte[4096]; // 4K is optimum
            // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
            // of the file, but does not waste memory.
            // The "using" will close the stream even if an exception occurs.
            using (FileStream streamWriter = File.Create(fullSubtitlePath))
            {
                StreamUtils.Copy(zipInputStream, streamWriter, buffer);
            }
        }



        private static string DetectSubtitleInZipEntry
            (ZipEntry zipEntry, string ext, string name)
        {
            string subtitleFilename = String.Empty;


            if (ext != ".srt" && ext != ".sub")
                return subtitleFilename;


            if (!name.Contains("cd")
                && !name.Contains("CD"))
            {
                subtitleFilename = zipEntry.Name;
                return subtitleFilename;
            }

            if (!name.Contains("1"))
                return subtitleFilename;


            subtitleFilename = zipEntry.Name;
            return subtitleFilename;
       
        }



//endof function






    }//endof class



}//endof namespace