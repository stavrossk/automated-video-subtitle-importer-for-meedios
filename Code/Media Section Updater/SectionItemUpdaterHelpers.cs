﻿using AVSI.Diagnostics;


namespace AVSI.SingleItemUpdaters
{


    class SectionItemUpdaterHelpers
    {



        internal static bool PerformDiagnosticOperations
            (
            ref bool fileServerChecked,
            ref bool fileServerIsOnline,
            string rootDirectory,  
            string location,
            ref bool isUnc,
            string fileExtension, 
            string[] videoExtensions,
            string[] videoExtensionsCommon)
        {


                if (!FileTypeVerifier.FileIsVideo
                   (videoExtensions, fileExtension,
                   videoExtensionsCommon))
                    return false;


            return true;
        
        }


    
    }//endof class


}//endof namespace
