﻿using System;
using System.IO;
using AMSI.ImportEngine;
using MeediOS;
using AVSI.OSDb;


namespace AVSI.SingleItemUpdaters
{



    class SingleMovieItemUpdater
    {



        internal static bool UpdateSectionItem
            (int itemId, IMLSection section,
            string pluginpath, ref int currentItem,
            int totalItems, string[] videoExtensions,
            string[] videoExtensionsCommon)
        {

            Helpers.LogMessageToFile("Calculating import progress...");
            ImportProgress.CurrentProgress =
            ImportProgress.CalculateImportProgress
                (currentItem, totalItems);


            #region item variables

            Helpers.LogMessageToFile("Locating item by ID: " + itemId);
            var item = section.FindItemByID(itemId);

            if (item == null)
                return true;

            Helpers.LogMessageToFile("Section entry was verified.");
            Helpers.LogMessageToFile("Initializing item keys...");

            string location = item.Location;
            string fileExtension;

            #endregion


            string rootDirectory;
            

            if ( !RetrieveFileInfo
                (location, out rootDirectory, 
                out fileExtension) )
                return true;



            if ( !Diagnostics.FileTypeVerifier.FileIsVideo
                (videoExtensions, fileExtension, videoExtensionsCommon) )
                return true;


            VideoHashConstructor.VideoFingerprintIdentifier.RetrieveFileHash
                    (item, location);


            if (ImportProgress.UserCancels(ImportProgress.SpecialStatus, item))
                return false;


            VideoSubtitleDownloaderHelpers.
                SearchForAndDownloadSubtitleForPrimaryOrSecondaryLanguage(item);
          


            if (ImportProgress.UserCancels(ImportProgress.SpecialStatus, item))
                return false;


            item.SaveTags();
            currentItem++;

            return true;


        }



        private static bool RetrieveFileInfo
            (string location, 
            out string rootDirectory,
            out string fileExtension)
        {

            fileExtension = String.Empty;
            rootDirectory = String.Empty;

            Helpers.LogMessageToFile(String.Format
            ("Validating media file {0}...",
            location));

            if (!File.Exists(location))
                return false;


            Helpers.LogMessageToFile(String.Format
            ("Attempting to retrieve information from media file {0}...",
            location));

            try
            {

            Helpers.LogMessageToFile("Creating FileInfo instance...");
            var fi = new FileInfo(location);

            Helpers.LogMessageToFile("Retrieving file extension...");
            fileExtension =  fi.Extension;

            Helpers.LogMessageToFile("Retrieving parent directory...");
            DirectoryInfo parent = fi.Directory;

            Helpers.LogMessageToFile("Retrieving root directory...");
            if (parent != null)
            {
                
                DirectoryInfo rootFi = parent.Root;
                rootDirectory = rootFi.FullName;
            }
            else rootDirectory = String.Empty;

            }
            catch (Exception e)
            {

                Helpers.LogMessageToFile(
                    String.Format
                    ("Unable to create filesystem" +
                    " instances for this media file." +
                    "An error occured: {0}", e));
                return false;
            }

            return true;

        }//endof func



    }//endof class


}//endof namespace
