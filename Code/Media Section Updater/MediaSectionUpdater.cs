﻿using System;
using System.Linq;
using AMSI.ImportEngine;
using AVSI.Diagnostics;
using AVSI.ImportEngine;
using AVSI.SingleItemUpdaters;
using MeediOS;

namespace AVSI
{



    class MediaSectionUpdater
    {
        internal static bool UpdateMediaSection
            (IMLSection section, string pluginPath)
        {


            Helpers.LogMessageToFile("Media Section Updater started.");

            int totalSectionItems;
            if (!CountSectionItems
                (section,
                out totalSectionItems))
                return true;



            #region Section varibales
            Helpers.LogMessageToFile
                ("Initializing section variables...");

            
            string[] videoExtensions;
            string[] videoExtensionsCommon;
            
            ImportProgress.CurrentProgress = 0;
            ImportProgress.GeneralStatus = "Updating Media Section";
            ImportProgress.SpecialStatus = "";

            MainImportingEngine.ThisProgress.Progress
            (ImportProgress.CurrentProgress,
            "Preparing to update media section...");
            //Thread.Sleep(500);
            
            #endregion


            FileTypeVerifier.CacheMediaExtensionsTables
               (pluginPath, out videoExtensions, out videoExtensionsCommon);


            return TryToPerfrormMediaSectionUpdate
                (section, pluginPath, videoExtensions,
                 videoExtensionsCommon, totalSectionItems);

        }


        private static bool TryToPerfrormMediaSectionUpdate
            (IMLSection section, string pluginPath, 
            string[] videoExtensions, 
            string[] videoExtensionsCommon, 
            int totalSectionItems)
        {

            try
            {

                int currentSectionItem = 1;
                section.BeginUpdate();
                Helpers.LogMessageToFile
                    ("Section update was started.");


                if (section.GetAllItemIDs().Any
                    (id => !SingleMovieItemUpdater.UpdateSectionItem
                        (id, section, pluginPath, ref currentSectionItem,
                         totalSectionItems, videoExtensions,
                         videoExtensionsCommon)))
                {
                    return false;
                }


                section.EndUpdate();
                Helpers.LogMessageToFile
                    ("Section update is complete.");


            }
            catch (Exception e)
            {
                AcknowledgeMediaSectionUpdaterErrorAndTerminateImportSequence(e);
            }

            return true;

        }





        private static bool CountSectionItems
            (IMLSection section, out int totalSectionItems)
        {

            Helpers.LogMessageToFile("Counting section items...");
            totalSectionItems = section.ItemCount;

            return totalSectionItems != 0;
        }



        private static void AcknowledgeMediaSectionUpdaterErrorAndTerminateImportSequence
            (Exception e)
        {

            Helpers.LogMessageToFile(Environment.NewLine +
                "An unexpected error occured " +
                "in Media Section Updater. The error was: " +
                Environment.NewLine + e + Environment.NewLine);


            ImportProgress.CurrentProgress = 100;

            MainImportingEngine.ThisProgress.Progress(ImportProgress.CurrentProgress,
                "The import process terminated unexpectidly due to an error.");

        }


    }



}
