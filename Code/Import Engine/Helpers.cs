﻿



using System;
using System.IO;
using MeediOS;
using MeediOS.Configuration;


namespace AVSI
{

    public static class Helpers
    {


        public static string GetTagValueFromItem(IMLItem item, string tagName)
       {

           string tagValue = String.Empty;
           try
           {
               tagValue = (string)item.Tags[tagName];
           }
           // ReSharper disable EmptyGeneralCatchClause
           catch (Exception)
           // ReSharper restore EmptyGeneralCatchClause
           { }

           return tagValue;
       }


        internal static string GetPluginPath()
        {
            if (ConfigurationManager.GetPluginsDirectory
                ("import", @"Automated Video Subtitle Importer\") != null)
            {
                string path = ConfigurationManager.GetPluginsDirectory
                    ("import", @"Automated Video Subtitle Importer\");
                return path;
            }

            return String.Empty;

        }


        internal static string GetMeedioRoot()
        {


            if (ConfigurationManager.GetRootDirectory(null) != null)
            {
                string path = ConfigurationManager.GetRootDirectory(null);
                return path;
            }

            return String.Empty;



        }


        internal static void LogMessageToFile(string msg)
        {

            if ( !Settings.EnableDebugLog)
                return;

            StreamWriter sw = File.AppendText(
                GetPluginPath() + "Debug.log");
            try
            {
                string logLine = String.Format(
                    "{0:G}: {1}", DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }


        }//endof function


        internal static void DeleteDebugLog(string debugLogPath)
        {
            try
            {
                File.Delete(debugLogPath);
            }
            catch (Exception e)
            {
                LogMessageToFile(
                    "An unexpected error occured while trying " +
                    "to delete the debug log file." +
                    " The error was: " + e);
            }
        }
    }//endof class




}//endof namespace
