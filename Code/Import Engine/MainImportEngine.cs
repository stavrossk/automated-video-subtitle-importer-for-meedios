﻿using AMSI.ImportEngine;
using AVSI.Diagnostics;
using System;
using MeediOS;


namespace AVSI.ImportEngine
{

    internal class MainImportingEngine
    {

        internal static bool DoImport(Importer importer,
            IMLSection section, IMLImportProgress progress)
        {

            try
            {


                #region INIT
                string pluginPath = Helpers.GetPluginPath();
                string debugLogPath = pluginPath + "Debug.log";

                //TODO: Replace ThisProgress with ImportProgress.CurrentProgress
                ThisProgress = progress;
                Settings.ImportingStarted = false;
                Settings.ImportingCompleted = false;

                ImportProgress.GeneralStatus = String.Empty;
                ImportProgress.SpecialStatus = String.Empty;
                ImportProgress.CurrentProgress = 0;

                #endregion

                Helpers.DeleteDebugLog(debugLogPath);


                if (!InternetConnectionDiagnostic.CheckOsDbAvailability())
                    return true;


                MediaSectionUpdater.UpdateMediaSection(section, pluginPath);



                ImportProgress.FinishImport();


                return true;

            }
            catch (Exception e)
            {

                Helpers.LogMessageToFile(
                    "An unexpected error ocurred " +
                    "in Main Import Engine. " +
                    "The error was: " + e);
            }


            return true;
        }


        internal static bool Initialized;
        internal static IMLImportProgress ThisProgress;
    }


}
