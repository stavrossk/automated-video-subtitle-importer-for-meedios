﻿using System;
using System.Windows.Forms;
using AVSI;
using AVSI.ImportEngine;
using MeediOS;

namespace AMSI.ImportEngine
{


    internal static class ImportProgress
    {



        internal static void UpdateProgress
            (string generalStatus,
            string specialStatus,
            IMLItem item)
        {

            Application.DoEvents();

            string importerText = String.IsNullOrEmpty(specialStatus)
                                      ? generalStatus : specialStatus;

            if (MainImportingEngine.ThisProgress == null)
                return;


            MainImportingEngine.ThisProgress.Progress
                (CurrentProgress, importerText);


        }



        internal static bool  UserCancels(string specialStatus, IMLItem item)
        {

            Application.DoEvents();

            if (MainImportingEngine.ThisProgress.Progress
                    (CurrentProgress, specialStatus) 
                && !Settings.UserCancels)
                return false;

            Application.DoEvents();

            CurrentProgress = 100;
            UpdateProgress("All operations were cancelled. " +
                           "Completed jobs were saved to library.", "", item);
            

            Application.DoEvents();
            
            return true;
        }



        internal static void FinishImport()
        {

            CurrentProgress = 100;
            MainImportingEngine.ThisProgress.Progress
                (CurrentProgress, 
                 "AVSI completed successfully!");

            Helpers.LogMessageToFile("AVSI completed successfully!");
        }



        internal static int CalculateImportProgress
            (int currentItemInt,
            int totalItemsInt)
        {
            double currentItem = Convert.ToDouble(currentItemInt);
            double totalItems = Convert.ToDouble(totalItemsInt);


            double divider = PerformDividerCalculation
                (totalItems, currentItem);


            double currentProgress = PerfromCurrentProgressCalculation
                (divider, totalItems, currentItem);


            int currentProgressInt 
                = Convert.ToInt32(currentProgress);


            return currentProgressInt;


        }




        private static double PerfromCurrentProgressCalculation
            (double divider, double totalItems, double currentItem)
        {

            double currentProgress;

            if (Math.Abs(currentItem - 0) < double.Epsilon
                || Math.Abs(totalItems - 0) < double.Epsilon
                || Math.Abs(divider - 0) < double.Epsilon)
                currentProgress = 0;
            else currentProgress = 100/divider;


            return currentProgress;
        }




        private static double PerformDividerCalculation
            (double totalItems, double currentItem)
        {
            double divider = 0;

            if (Math.Abs(currentItem - 0) > double.Epsilon)
                divider = totalItems/currentItem;

            return divider;
        }

        internal static int CurrentProgress;
        internal static string GeneralStatus = String.Empty;
        internal static string SpecialStatus = String.Empty;
    }



}
