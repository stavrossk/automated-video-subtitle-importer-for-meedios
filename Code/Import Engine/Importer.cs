﻿


using System;
using System.Windows.Forms;
using AVSI.ImportEngine;
using CookComputing.XmlRpc;
using MeediOS;
using MeediOS.Cache;



public struct SearchParams
{
    public string SubtitleLanguageId;
    //public string moviehash;
    //public double moviesize;
    public string Imdbid;
}

public struct LoginResult
{
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public string Token;
    public string Status;
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public double Seconds;
}





namespace AVSI
{

    public class Importer : IMLImportPlugin
    {

        public bool Import(IMLSection section, IMLImportProgress progress)
        {
            return MainImportingEngine.DoImport(this, section, progress);
        }


        public bool EditCustomProperty(IntPtr window, string propertyName, ref string value)
        {

            return false;
        }


        /// <summary>
        /// Retrieving config settings from configuration.exe and at startup from MeediOSApp 
        /// </summary>
        /// <PARAM name="properties">Containing the list of settings</PARAM><PARAM name="errorText">Returns the error text</PARAM>
        /// <returns>
        /// Returns true on success, false on error.
        /// </returns>
        public bool SetProperties(IMeedioItem properties, out string errorText)
        {
            errorText = null;

            try
            {
                PropertiesSetter.SetProperties(properties);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            return true;
        }


        /// <summary>
        /// Defining config entries
        /// </summary>
        /// <PARAM name="propertyIndex">Parameter number</PARAM><PARAM name="propertyDefinition">Parameter definition</PARAM>
        /// <returns>
        /// Return true if propertyDefinition is filled with valid data, false if no property with index propertyIndex exists.
        /// </returns>
        public bool GetProperty(int index, IMeedioPluginProperty prop)
        {


                int counter = 1;


                if (index == counter++)
                {
                    //set the internal name
                    prop.Name = "LanguageProp";

                    prop.GroupCaption = "Subtitle Languages";
                    //set name shown to user
                    prop.Caption = "Primary subtitle language:";
                    prop.Caption = TranslationProvider.Translate(prop.Caption, this);
                    
                    //set the tool tip
                    prop.HelpText = "Please insert the ISO639 3-letter code of your country." 
                                    + Environment.NewLine +
                                    "AVSI will try to download subtitles for this language first." 
                                    + Environment.NewLine +
                                    "To find your country code please use this reference:" +
                                    " http://en.wikipedia.org/wiki/List_of_ISO_639-2_codes ";

                    prop.DefaultValue = Settings.PrimaryLanguage;
                    prop.DataType = "string";
                    
                    return true;
                
                }


                if (index == counter++)
                {
                    prop.GroupCaption = "Subtitle Languages";
                    //set the internal name
                    prop.Name = "LanguagePropSecondary";
                    //set name shown to user
                    prop.Caption = "Secondary subtitle language:";
                    prop.Caption = TranslationProvider.Translate(prop.Caption, this);
                    //set the tool tip
                    prop.HelpText =
                        "If no subtitles are found for your primary language," +
                        " AVSI will search for and download subtitles for this language.";
                    prop.DefaultValue = Settings.SecondaryLanguage;
                    prop.DataType = "string";
                    return true;
                }




                if (index == counter++)
                {
                    prop.GroupCaption = "Diagnostics";
                    //set the internal name
                    prop.Name = "FileTypeVerifierProp";

                    //set name shown to user
                    prop.Caption = "Enable File Type Verifier";
                    prop.Caption = TranslationProvider.Translate(prop.Caption, this);
                    //set the tool tip

                    prop.HelpText =
                        "If this option is enabled, AVSI will try to verify that" 
                        + Environment.NewLine + 
                        "each media section's entry corresponds to an actual video file."
                        + Environment.NewLine +
                        " This check uses a file extension table"
                        + Environment.NewLine +
                        "located in the plugin's directory which contains" 
                        + Environment.NewLine + 
                        " the most common video file extensions.";

                    prop.DefaultValue = Settings.EnableFileTypeVerifier;
                    prop.DataType = "bool";

                    return true;

                }




// ReSharper disable RedundantAssignment
                if (index == counter++)
// ReSharper restore RedundantAssignment
                {

                    prop.GroupCaption = "Diagnostics";
                    //set the internal name
                    prop.Name = "DebugLogProp";
                    //set name shown to user
                    prop.Caption = "Write Debug.log";
                    prop.Caption = TranslationProvider.Translate(prop.Caption, this);
                    //set the tool tip

                    prop.HelpText =
                        "With this option enabled, a log file named 'Debug.log' " 
                        + Environment.NewLine + 
                        "will be written in the plugin's directory" + 
                        " containing debug information." 
                        + Environment.NewLine + 
                        " In case something goes wrong, " +
                        "please provide this file with your bug/issue report.";

                    prop.DefaultValue = Settings.EnableDebugLog;
                    prop.DataType = "bool";

                    return true;

                }      

            return false;
        }






    }//endof  class


}//endof namespace