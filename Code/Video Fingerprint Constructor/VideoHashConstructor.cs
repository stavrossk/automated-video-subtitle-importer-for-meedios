﻿using System;
using System.Threading;
using AMSI.ImportEngine;
using AVSI.ImportEngine;
using MeediOS;

namespace AVSI.VideoHashConstructor
{


    class VideoFingerprintIdentifier
    {


        internal static void RetrieveFileHash
            (IMLItem item, string location)
        {

            if (CheckForExistingHash(item)) 
                return;



            CalculateAndSaveHashToLibrary
                (item, location);
      
        }

        private static void CalculateAndSaveHashToLibrary
            (IMLItem item, string location)
        {

            Helpers.LogMessageToFile(String.Format
               ("Calculating video fingerprint for {0}...", item.Name));

            MainImportingEngine.ThisProgress.Progress
                (ImportProgress.CurrentProgress,
                 String.Format("Calculating video fingerprint for {0}...",
                 item.Name));

            Thread.Sleep(200);

            string videoHash = Hasher.CalculateFileHash(location, item);

            item.Tags["Hash"] = videoHash;
            item.SaveTags();
        }



        private static bool CheckForExistingHash(IMLItem item)
        {
            string videoHash = Helpers.GetTagValueFromItem(item, "Hash");
            return !String.IsNullOrEmpty(videoHash);
        }


    }




}
