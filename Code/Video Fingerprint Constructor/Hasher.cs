﻿



using System;
using System.Text;
using System.IO;
using MeediOS;


namespace AVSI
{


    public class Hasher
    {


        private static byte[] ComputeMovieHash
            (string filename)
        {

            try
            {
                Stream input = File.OpenRead(filename);
                byte[] result = ComputeMovieHash(input);
                return result;
            }
            catch(Exception e)
            {

                Helpers.LogMessageToFile
                    ("An error occured while attempting" +
                     " to compute video hash. The error was: " + e );

                return null;
            }

        }




        private static byte[] ComputeMovieHash(Stream input)
        {


            long streamsize = input.Length;
            long lhash = streamsize;

            long i = 0;

            var buffer 
                = new byte[sizeof(long)];
            
            while (i < 65536 / sizeof(long)
                && (input.Read(buffer, 0, sizeof(long)) > 0))
            {

                i++;
                lhash += BitConverter.ToInt64(buffer, 0);

            }


            input.Position = Math.Max(0, streamsize - 65536);
            i = 0;
            while (i < 65536 / sizeof(long)
                && (input.Read(buffer, 0, sizeof(long)) > 0))
            {

                i++;
                lhash += BitConverter.ToInt64(buffer, 0);
            
            }


            input.Close();
            byte[] result = BitConverter.GetBytes(lhash);
            Array.Reverse(result);
            return result;
        }



        public static string ToHexadecimal(byte[] bytes)
        {

            var hexBuilder
                = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                hexBuilder.Append(bytes[i].ToString("x2"));
            }
            return hexBuilder.ToString();
        }



        public static string CalculateFileHash
            (string location, IMLItem item)
        {

            byte[] moviehashtmp 
                = ComputeMovieHash(@location);

            if (moviehashtmp == null)
                return string.Empty;

            string moviehash
                = ToHexadecimal(moviehashtmp);

            item.Tags["Hash"] = moviehash;

            return moviehash;
        }



    }



}
