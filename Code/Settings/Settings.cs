﻿

using System;

namespace AVSI
{


    internal class Settings
    {
        public static string PrimaryLanguage = "eng";
        public static string SecondaryLanguage = String.Empty;
        public static bool EnableFileTypeVerifier = true;
        public static bool EnableDebugLog = true;

        #region Current Session Variables
        internal static bool UserCancels;
        internal static bool ImportingStarted;
        internal static bool ImportingCompleted;
        #endregion

        
    }



}
