﻿
using System;
using System.Windows.Forms;
using AVSI.ImporterPropertiesGroups;
using MeediOS;


namespace AVSI
{
    internal static class ImporterProperties
    {

        public static bool GetProperty(Int32 index, IMeedioPluginProperty prop)
        {


            #region init vars
            int counter = 1;

            string[] threechoices = new string[3];
            string[] twochoices = new string[2];
            string[] fourchoices = new string[4];


            #endregion



            try
            {

                if (MediaUpdatersProperties.GetMediaUpdatersProperties
                    (this, index, prop, twochoices,
                    threechoices, fourchoices, ref counter))
                    return true;


                if (DiagnosticsProperties.GetDiagnosticsProperties
                    (this, index, prop, ref counter)) 
                    return true;

            
            
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }


            return false;


        }


//endof function


    } //endof class


} //endof namespace