﻿using System;
using MeediOS;
using MeediOS.Cache;

namespace AVSI.ImporterPropertiesGroups
{
    class DiagnosticsProperties
    {
        internal bool GetDiagnosticsProperties
            (ImporterProperties importerProperties,
            int index,  IMeedioPluginProperty prop, ref int counter)
        {


            #region Diagnostics Properties

            //if (index == counter++)
            //{
          
            //    //set the internal name
            //    prop.Name = "ConnectionDiagnosticsProp";
            //    //set name shown to user
            //    prop.Caption = "Enable internet connection diagnostic? ";
            //    prop.Caption = TranslationProvider.Translate(prop.Caption, importerProperties);
            //    prop.GroupCaption = "                               Diagnostics";
                
            //    //set the tool tip
            //    prop.HelpText =
            //        "Should AVSI test for internet connection and online databases availability before attempting to download data?" +
            //        Environment.NewLine +
            //        "This feature will help the importer recognize offline or problematic sources and avoid using them," +
            //        Environment.NewLine +
            //        "preventing failed retries, minimizing delays and improving importing times in those situations.";
                
            //    prop.DefaultValue = Settings.ConnectionDiagnosticsEnabled;
            //    prop.DataType = "bool";
            //    return true;

            //}


            //if (index == counter++)
            //{
            //    //set the internal name
            //    prop.Name = "DisableSourcesProp";
            //    //set name shown to user
            //    prop.Caption = "Disable problematic sources? ";
                
            //    prop.Caption = TranslationProvider.Translate(prop.Caption, importerProperties);
            //    //set the tool tip
            //    prop.HelpText = " Should unconnectable online sources be disabled " +
            //                    "for the rest of an importing session? ";
            //    prop.DataType = "bool";
            //    return true;
            //}

            if (index == counter++)
            {
                prop.GroupCaption = "                               Diagnostics";
                //set the internal name
                prop.Name = "FileserverDiagnosticsProp";

                //set name shown to user
                prop.Caption = "Enable media server diagnostic? ";
                prop.Caption = TranslationProvider.Translate(prop.Caption, importerProperties);
                //set the tool tip
               
                prop.HelpText =
                    "Should AVSI check if your network disk is online and accessible before attempting to read or transfer any data?" +
                    Environment.NewLine +
                    "This feature will prevent failed attempts to access network disks and improve importing times in those cases.";
                
                prop.DefaultValue = Settings.WantFileserverDiagnostics;
                prop.DataType = "bool";
                
                return true;
            
            }

            #endregion

            return false;
        }


    }


}
