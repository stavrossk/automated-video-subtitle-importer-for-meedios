﻿using System;
using System.Collections.Generic;
using MeediOS.Cache;
using MeediOS;




namespace AVSI.Code.Importer_Properties
{


    class SubtitleDownloaderProperties
    {

        
        internal static bool GetSubtitleDownloaderProperties(ImporterProperties importerProperties, 
            int index, IMeedioPluginProperty  prop, IList<string> twochoices, ref int counter)
        {


            if (index == counter++)
            {
                //set the internal name
                prop.Name = "LanguageProp";

                prop.GroupCaption = "                             Subtitle Downloader Settings";
                //set name shown to user
                prop.Caption = "Primary Subtitles language:";
                prop.Caption = TranslationProvider.Translate(prop.Caption, importerProperties);
                //set the tool tip
                prop.HelpText = "Please insert the ISO639 3-letter code of your country." + Environment.NewLine +
                                "AVSI will try to download subtitles for this language first." + Environment.NewLine +
                                "To find your country code use this reference: http://en.wikipedia.org/wiki/List_of_ISO_639-2_codes ";
                prop.DefaultValue = Settings.PrimaryLanguage;
                prop.DataType = "string";
                return true;
            }


            if (index == counter++)
            {
                //set the internal name
                prop.Name = "LanguagePropSecondary";
                //set name shown to user
                prop.Caption = "Secondary subtitles language:";
                prop.Caption = TranslationProvider.Translate(prop.Caption, importerProperties);
                //set the tool tip
                prop.HelpText =
                    "If no subtitles are found for your primary language," +
                    " AVSI will search for subtitles for this language.";
                prop.DefaultValue = Settings.SecondaryLanguage;
                prop.DataType = "string";
                return true;
            }


            return false;
        }
    }
}
