using MeediOS;


namespace AVSI
{


    public class PropertiesSetter
    {


        internal static void SetProperties(IMeedioItem properties)
        {

            if (properties["LanguageProp"] != null)
                Settings.PrimaryLanguage
                    = (string) properties["LanguageProp"];
            
            if (properties["LanguagePropSecondary"] != null)
                Settings.SecondaryLanguage
                    = (string) properties["LanguagePropSecondary"];


            if (properties["FileTypeVerifierProp"] != null)
                Settings.EnableFileTypeVerifier =
                    (bool)properties["FileTypeVerifierProp"];
 

            if (properties["DebugLogProp"] != null) 
                Settings.EnableDebugLog = 
                    (bool) properties["DebugLogProp"];
           


        }


    }


}