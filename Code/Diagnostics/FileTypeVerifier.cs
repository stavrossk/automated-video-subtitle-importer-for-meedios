﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AMSI.ImportEngine;



namespace AVSI.Diagnostics
{


    class FileTypeVerifier
    { 


        internal static bool FileIsVideo
            (IEnumerable<string> videoExtensions,
             string fileExtension,
            IEnumerable<string> videoExtensionsCommon  )
        {

            if (!Settings.EnableFileTypeVerifier)
                return true;

            if (String.IsNullOrEmpty
                (fileExtension))
                return true;


            videoExtensionsCommon
                = videoExtensionsCommon.ToList();
            
            videoExtensions 
                = videoExtensions.ToList();
     

            if (CheckForEmptyFileExtensionLists
                (videoExtensions,
                videoExtensionsCommon))
                return true;


            return LocateExtensionInLists
                (videoExtensions, fileExtension,
                videoExtensionsCommon);

        }



        private static bool CheckForEmptyFileExtensionLists
            (IEnumerable<string> videoExtensions,
            IEnumerable<string> videoExtensionsCommon)
        {

            if (!videoExtensionsCommon.Any())
                return true;

            return !videoExtensions.Any();
        }


        private static bool LocateExtensionInLists
            (IEnumerable<string> videoExtensions,
            string fileExtension,
            IEnumerable<string> videoExtensionsCommon)
        {


            ImportProgress.UpdateProgress
                ("Performing Diagnostic Operations...",
                 "Detecting file's media type...", null);


            if (LocateFileExtensionInSpecifiedList
                (videoExtensionsCommon, fileExtension))
                return true;


            return LocateFileExtensionInSpecifiedList
                (videoExtensions, fileExtension);
        }



        internal static bool LocateFileExtensionInSpecifiedList
            (IEnumerable<string> fileExtensionsList, string ext)
        {

            bool extensionFoundInList = false;

// ReSharper disable UnusedVariable
            foreach (string videoext 
// ReSharper restore UnusedVariable
                in fileExtensionsList.Where
                (videoext => videoext == ext))
                extensionFoundInList = true;

            return extensionFoundInList;

        }



        internal static void CacheMediaExtensionsTables
            (string pluginpath, out string[] videoExtensions,
             out string[] videoExtensionsCommon)
        {


            ImportProgress.UpdateProgress
                ("Performing Diagnostic Operations",
                "Loading media extension tables...", null);
            
            Helpers.LogMessageToFile("Loading video extension tables...");

            

            videoExtensions = File.Exists
            (String.Format("{0}video extensions.txt", pluginpath))
            ? File.ReadAllLines(String.Format("{0}video extensions.txt", pluginpath))
            : null;

            videoExtensionsCommon = File.Exists
            (String.Format("{0}video extensions common.txt", pluginpath))
            ? File.ReadAllLines(String.Format("{0}video extensions common.txt", pluginpath))
            : null;


        }


    }



}
