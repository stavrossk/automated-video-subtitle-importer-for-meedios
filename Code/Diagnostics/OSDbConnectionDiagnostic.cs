﻿using System;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using AMSI.ImportEngine;


// ReSharper disable CheckNamespace
namespace AVSI.Diagnostics
// ReSharper restore CheckNamespace
{


    class InternetConnectionDiagnostic
    {


        internal static bool CheckOsDbAvailability()
        {


            
            ImportProgress.UpdateProgress
                ("Performing Diagnostic Operations...",
                 "Checking OpenSubtitles.org...", null);


            Thread.Sleep(500);
            bool osDbIsOnline;


            try
            {
             
                osDbIsOnline = Ping
                    ("www.opensubtitles.org");


            }
            catch (Exception e)
            {

                ImportProgress.UpdateProgress
                    ("Performing Diagnostic Operations...",
                     "OSdb is offline." +
                     " The import process" +
                     " will not continue.", null);


                osDbIsOnline = false;

                Helpers.LogMessageToFile
                    (e.ToString());

            }


            return osDbIsOnline;
        }




        internal static bool Ping(string hostname)
        {


            var pingSender
                = new Ping();

            var options
                = new PingOptions();


            var buffer = CreateTransmitBuffer();


            //TODO: Move the Ping timeout to the settings file 
            const int timeout = 10000;


            if (PerformPingCheckReply
                (hostname, buffer,
                options, pingSender,
                timeout))
                return true;


            if (PerformPingCheckReply
                (hostname, buffer,
                options, pingSender,
                timeout))
                return true;



            pingSender.Dispose();            
            return false;

        }



        private static bool PerformPingCheckReply
            (string hostname, byte[] buffer,
            PingOptions options, Ping pingSender,
            int timeout)
        {


            PingReply reply
                = pingSender.Send
                    (hostname, timeout,
                     buffer, options);


            if (reply != null
                && reply.Status
                == IPStatus.Success)
            {
                pingSender.Dispose();
                return true;
            }


            return false;
        }



        private static byte[] CreateTransmitBuffer()
        {


            // Create a buffer of 32 bytes of data to be transmitted.
            const string data
                = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            byte[] buffer
                = Encoding.ASCII.GetBytes(data);
            
            return buffer;
        }



    }



}
