﻿using System.IO;
using System.Threading;
using AMSI.ImportEngine;

namespace AVSI.Diagnostics
{


    //TODO: File Server Diagnostic needs to be rewritten in order to cope with multiple UNC roots (file servers)
 
    class FileServerChecker
    {


        internal static bool CheckFileServer
            (string location,
             string rootDirectory)
        {

            if (!Settings.EnableFileserverDiagnostic)
                return true;


            bool pathIsUnc = location.StartsWith("\\");

            if (!pathIsUnc)
                return true;


            if (string.IsNullOrEmpty(rootDirectory))
                return true;

          


            Helpers.LogMessageToFile("Checking file server availability..");

            ImportProgress.UpdateProgress("Performing Diagnostic operations",
                "Checking file server availability...", null);


            bool fileServerIsOnline = Directory.Exists(rootDirectory);


            if (!fileServerIsOnline)
            {

                Helpers.LogMessageToFile
                    ("Your file server is offline. " +
                    "AVSI will skip processing this item.");

                ImportProgress.UpdateProgress
                    ("Performing Diagnostic operations",
                    "Your file server is offline." +
                    " AVSI will processing this item.", null);

                Thread.Sleep(100);
                return false;
            }


            Helpers.LogMessageToFile("File server is online.");
            return true;

        }



    }



}
