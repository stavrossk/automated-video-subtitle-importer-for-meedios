Automated Video Subtitle Importer v1.2 Alpha

Copyright (C) 2012 Stavros Skamagkis




An import plugin for Meedio/MeediOS home theater frontend applications.





Automated Video Subtitle Importer is an import type plugin for MeediOS capable of
 downloading synchronized subtitles for your video collection in your prefered language(s).


  A.V.S.I. calculates and constructs a unique alphanumeric key
 for each of your video files, called "hash" or "fingerprint".


   Then using this unique key, A.V.S.I. interfaces with opensubtitles.org
 subtitle database to automatically find and download subtitles
 which will be perfectly synchronized to the version of your video file.




	-------------- Features -------------


  ====  Completely automated and ultra fast subtitle retrieval ====
 
  No more searching for hours and hours for subtitles for your videos by hand.
 A.V.S.I. fast subtitle retrieval engine will download synchronized subtitles
 for your videos at a rate of up to 80 per minute.



  ==== Subtitle not found for your primary language? Try a second one: ====
 
  Additionaly to your prefered primary subtitle language,
 A.V.S.I. supports a secondary "backup" language for which will download a subtitle
 in the event that no subtitle was found for the primary language.





  ==== If it isn't needed it shouldn't be done: ====
 
   Did you just run A.V.S.I. against your e-books library by mistake?
 A.V.S.I. will try it's best to avoid unecessary file hash calculations
 and OSdb online requests, by skipping files that are not videos,
 according to their file extension.[/list]






  ------------------- Quick Start -------------------------------------------------

[*] Download the plugin from OpenMAID and run the installer to install the plugin.


[*] Use a media file importer to populate your MeediOS video library.
   Recommended file importers: 

  MoviesFileImporter for movies, 
  TvShowsFileImporter for tv shows, 
  or MediaFairy for MeediOS Lite Edition for both movies and tv shows.


[*] Attach an instance of A.V.S.I. to your populated video library,
 go to the importer's Properties tab and set your prefered
 primary and secondary subtitle languages.


[*] Run the importer and enjoy your videos with perfectly synchronized subtitles!



------------------------------------------------------------------------------------