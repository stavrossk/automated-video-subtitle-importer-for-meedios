/* 
XML-RPC.NET library
Copyright (c) 2001-2006, Charles Cook <charlescook@cookcomputing.com>

Permission is hereby granted, free of charge, to any person 
obtaining a copy of this software and associated documentation 
files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, 
publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be 
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.
*/

namespace CookComputing.XmlRpc
{
  using System;
  using System.IO;
  using System.Net;
  using System.Reflection;
  using System.Text;
  using System.Threading;

  public class XmlRpcAsyncResult : IAsyncResult
  {
    // IAsyncResult members
    public object AsyncState 
    { 
      get { return userAsyncState; } 
    }

    public WaitHandle AsyncWaitHandle 
    {
      get
      {
        bool completed = _isCompleted;
        if (_manualResetEvent == null)
        {
          lock(this)
          {
            if (_manualResetEvent == null)
              _manualResetEvent = new ManualResetEvent(completed);
          }
        }
        if (!completed && _isCompleted)
          _manualResetEvent.Set();
        return _manualResetEvent;
      }
    }

    public bool CompletedSynchronously 
    { 
      get { return _completedSynchronously; } 
      set 
      { 
        if (_completedSynchronously)
          _completedSynchronously = value;
      }
    }

    public bool IsCompleted 
    {
      get { return _isCompleted; } 
    }

#if (!COMPACT_FRAMEWORK)
    public CookieCollection ResponseCookies
    {
      get { return _responseCookies; }
    }
#endif

#if (!COMPACT_FRAMEWORK)
    public WebHeaderCollection ResponseHeaders
    {
      get { return _responseHeaders; }
    }
#endif


    public bool UseEmptyParamsTag
    {
      get { return _useEmptyParamsTag; }
    }

    public bool UseIndentation 
    {
      get { return _useIndentation; } 
    }

    public int Indentation 
    {
      get { return _indentation; } 
    }

      public bool UseIntTag { get; private set; }

      public bool UseStringTag { get; private set; }

      // public members
    public void Abort()
    {
      if (request != null)
        request.Abort();
    }

    public Exception Exception 
    {
      get { return exception; } 
    }

    public XmlRpcClientProtocol ClientProtocol 
    { 
      get { return clientProtocol; } 
    }

    //internal members
    internal XmlRpcAsyncResult(
      XmlRpcClientProtocol ClientProtocol, 
      XmlRpcRequest XmlRpcReq, 
      Encoding XmlEncoding,
      bool useEmptyParamsTag,
      bool useIndentation,
      int indentation,
      bool UseIntTag,
      bool UseStringTag,
      WebRequest Request, 
      AsyncCallback UserCallback, 
      object UserAsyncState, 
      int retryNumber)
    {
      xmlRpcRequest = XmlRpcReq;
      clientProtocol = ClientProtocol;
      request = Request;
      userAsyncState = UserAsyncState;
      userCallback = UserCallback;
      _completedSynchronously = true;
      xmlEncoding = XmlEncoding;
      _useEmptyParamsTag = useEmptyParamsTag;
      _useIndentation = useIndentation;
      _indentation = indentation;
      this.UseIntTag = UseIntTag;
      this.UseStringTag = UseStringTag;
    }
  
    internal void Complete(
      Exception ex)
    {
      exception = ex;
      Complete();
    }

    internal void Complete()
    {
      try
      {
        if (responseStream != null)
        {
          responseStream.Close();
          responseStream = null;
        }
        if (responseBufferedStream != null)
          responseBufferedStream.Position = 0;
      }
      catch(Exception ex)
      {
        if (exception == null)
          exception = ex;
      }
      _isCompleted = true;
      try
      {
        if (_manualResetEvent != null)
          _manualResetEvent.Set();
      }
      catch(Exception ex)
      {
        if (exception == null)
          exception = ex;
      }
      if (userCallback != null)
        userCallback(this);
    }

    internal WebResponse WaitForResponse()
    {
      if (!_isCompleted)
        AsyncWaitHandle.WaitOne();
      if (exception != null)
        throw exception;
      return response;
    }

    internal bool EndSendCalled 
    { 
      get { return _endSendCalled; } 
      set { _endSendCalled = value; }
    }

    internal byte[] Buffer 
    { 
      get { return buffer; } 
      set { buffer = value; }
    }

    internal WebRequest Request { get { return request; } 
    }

    internal WebResponse Response 
    { 
      get { return response; } 
      set { response = value; }
    }

    internal Stream ResponseStream
    { 
      get { return responseStream; } 
      set { responseStream = value; }
    }

    internal XmlRpcRequest XmlRpcRequest
    { 
      get { return xmlRpcRequest; } 
      set { xmlRpcRequest = value; }
    }

    internal Stream ResponseBufferedStream
    { 
      get { return responseBufferedStream; } 
      set { responseBufferedStream = value; }
    }

    internal Encoding XmlEncoding
    {
      get { return xmlEncoding; } 
    }

      readonly XmlRpcClientProtocol clientProtocol;
      readonly WebRequest request;
      readonly AsyncCallback userCallback;
      readonly object userAsyncState;
    bool _completedSynchronously;
    bool _isCompleted;
    bool _endSendCalled;
    ManualResetEvent _manualResetEvent;
    Exception exception;
    WebResponse response;
    Stream responseStream;
    Stream responseBufferedStream;
    byte[] buffer;
    XmlRpcRequest xmlRpcRequest;
    Encoding xmlEncoding;
    bool _useEmptyParamsTag;
    bool _useIndentation;
    int _indentation;
#if (!COMPACT_FRAMEWORK)
    internal CookieCollection _responseCookies;
    internal WebHeaderCollection _responseHeaders;
#endif
  }
}