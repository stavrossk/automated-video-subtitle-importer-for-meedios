﻿



  --> Version 1.1 Alpha

  * Changed the name of the library field which stores the video file's hash from "VideoHash" to "Hash" for compatibility with MediaInfoImporter.

  * File Server Diagnostic removed for now as it needs to be rewritten to support multiple file servers or UNC paths.

  * Made a few code optimizations for improved performance and cleaner operation.

  * Added a plugin icon.